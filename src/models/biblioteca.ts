export class Biblioteca {
    _id : number;
    titulo: string;
    anio: number;
    edicion: number;
    editorial: string;
    nombreAutor: string;
    numeroHojas: number;

    constructor(_id: number, titulo: string, anio: number, edicion: number, editorial: string, nombreAutor: string, numeroHojas: number  ){
        this._id = _id;
        this.titulo = titulo;
        this.anio = anio;
        this.edicion = edicion;
        this.editorial = editorial;
        this.nombreAutor = nombreAutor;
        this.numeroHojas = numeroHojas;
    }
}