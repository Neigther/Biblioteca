import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
// import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.css']
})
export class CrearComponent implements OnInit {
  // bibliotecaForm: FormGroup;

  // constructor(private fb: FormBuilder) {
    // this.bibliotecaForm = this.fb.group({

    // })
  //  }

  conf(){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Se agregó correctamente',
      showConfirmButton: false,
      timer: 1900
    })
  }

  ngOnInit(): void {
    
  }

}
