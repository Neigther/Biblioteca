import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  conf(){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Se agregó correctamente',
      showConfirmButton: false,
      timer: 1900
    })
  }

  act(){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Se actualizó correctamente',
      showConfirmButton: false,
      timer: 1900
    })
  }

  del(){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Se eliminó el registro correctamente',
      showConfirmButton: false,
      timer: 1900
    })
  }
}
