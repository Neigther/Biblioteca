import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarprincipalComponent } from './components/navbarprincipal/navbarprincipal.component';
import { BibliotecaComponent } from './components/biblioteca/biblioteca.component';
import { CrearComponent } from './components/crear/crear.component';
import { ConsultarComponent } from './components/consultar/consultar.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavbarprincipalComponent,
    BibliotecaComponent,
    CrearComponent,
    ConsultarComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
